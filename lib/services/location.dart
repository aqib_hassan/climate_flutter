import 'package:geolocator/geolocator.dart';

class Location {
  Future<Position?> getCurrentLocation() async {
    try {
      LocationPermission permission = await Geolocator.checkPermission();
      if (permission.name == 'whileInUse' || permission.name == 'always') {
        Position position = await Geolocator.getCurrentPosition(
            desiredAccuracy: LocationAccuracy.low);
        return position;
      } else if (permission.name == 'deniedForever') {
        await Geolocator.openLocationSettings();
      } else {
        LocationPermission permission = await Geolocator.requestPermission();
        Position position = await Geolocator.getCurrentPosition(
            desiredAccuracy: LocationAccuracy.low);
        return position;
      }
    } catch (e) {
      print(e);
    }
    return null;
  }
}
